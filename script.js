

let normalLevelClicked = document.querySelector(".normalLevel");
normalLevelClicked.addEventListener("click", (e) => {
  normalLevelGame()
})

function normalLevelGame() {
  let mainContainerDiv = document.querySelector(".mainContainer");
  mainContainerDiv.style.display = "none";

  const COLORS = [
    "red",
    "blue",
    "green",
    "orange",
    "purple",
    "red",
    "blue",
    "green",
    "orange",
    "purple"
  ];
  function shuffle(array) {
    let counter = array.length;

    // While there are elements in the array
    while (counter > 0) {
      // Pick a random index
      let index = Math.floor(Math.random() * counter);

      // Decrease counter by 1
      counter--;

      // And swap the last element with it
      let temp = array[counter];
      array[counter] = array[index];
      array[index] = temp;
    }

    return array;
  }

  let shuffledColors = shuffle(COLORS);
  let body = document.body;
  let CardsContainerDiv = document.createElement("div");
  CardsContainerDiv.className = "cardsContainerDiv";
  CardsContainerDiv.style.marginTop = "5vh";
  CardsContainerDiv.style.margiLeft = "5vw";
  CardsContainerDiv.style.display = "flex";
  CardsContainerDiv.style.flexWrap = "wrap";
  CardsContainerDiv.style.justifyContent = "space-around";
  CardsContainerDiv.style.height = "100vh";
  CardsContainerDiv.style.backgroundColor = "grey";

  body.prepend(CardsContainerDiv);

  let cardCount = 0;
  shuffledColors.map(color => {
    const individualCard = document.createElement("div");
    //individualCard.style.backgroundColor = color;
    individualCard.id = `card${cardCount}`;
    individualCard.className = color;
    individualCard.style.backgroundColor = "whitesmoke";
    individualCard.style.marginBottom = "10vh";
    individualCard.style.marginTop = "10vh";
    individualCard.style.width = "18vw";
    individualCard.style.height = "30vh";
    CardsContainerDiv.appendChild(individualCard);
    cardCount += 1;
  });

  let firstClicked;
  let secondClicked;
  let count = 0;
  let matchedCardsId=[];

  let cardIdarray = [];
  let clickedOnCards = document.querySelector(".cardsContainerDiv")
  clickedOnCards.addEventListener("click", (e) => {
    if ((e.target != clickedOnCards) &(!matchedCardsId.includes(e.target.id))){
      console.log("clicked");
      cardIdarray.push(e.target.id);

      if (count == 0) {
        e.target.style.backgroundColor = e.target.className;
        firstClicked = e.target.className;
        count += 1;
      }
      else if (count == 1) {
        e.target.style.backgroundColor = e.target.className;
        secondClicked = e.target.className;
        count += 1;
      }

      if ((firstClicked == secondClicked) & (count == 2)) {
        if(cardIdarray[0]==cardIdarray[1]){

          console.log("try again")
          let firstClickedClass = document.getElementsByClassName(firstClicked);
          let secondClickedClass = document.getElementsByClassName(secondClicked);
          count = 0;
          cardIdarray.pop();
          cardIdarray.pop();

          setTimeout(() => {
            firstClickedClass[0].style.backgroundColor = "white";
            firstClickedClass[1].style.backgroundColor = "white";

            secondClickedClass[0].style.backgroundColor = "white";
            secondClickedClass[1].style.backgroundColor = "white";


          }, 1000)


        }
        else {
          count = 0;
          let secondId = cardIdarray.pop();
          let firstId = cardIdarray.pop();
          matchedCardsId.push(firstId);
          matchedCardsId.push(secondId);
          

        }
      }
      if ((firstClicked != secondClicked) & (count == 2)) {
        count = 0;
        cardIdarray.pop();
        cardIdarray.pop();
        let firstClickedClass = document.getElementsByClassName(firstClicked);
        let secondClickedClass = document.getElementsByClassName(secondClicked);

        setTimeout(() => {
          firstClickedClass[0].style.backgroundColor = "white";
          firstClickedClass[1].style.backgroundColor = "white";

          secondClickedClass[0].style.backgroundColor = "white";
          secondClickedClass[1].style.backgroundColor = "white";


        }, 1000)
      }


    }
  })

}

